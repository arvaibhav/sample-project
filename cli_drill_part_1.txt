mkdir -p hello/five/six/seven hello/one/two/three/four
touch hello/five/six/c.txt hello/five/six/seven/error.log
touch hello/one/a.txt hello/one/b.txt hello/one/two/d.txt hello/one/two/three/e.txt hello/one/two/three/four/access.log
ch hello
find -name "*.log"
rm -rf ./five/six/seven/error.log
rm -rf ./one/two/three/four/access.log

echo "Unix is a family of multitasking, multiuser computer operating systems that derive from the original AT&T Unix, development starting in the 1970s at the Bell Labs research center by Ken Thompson, Dennis Ritchie, and others." >> ./one/a.txt
 
rm -rf five
 
